// 识别的所有js
let origins = {
    common: 'nodeModules_eeb5887.js',
    framework: 'public_10b9d27.js'
};

// 重定向的js
let redirects = {
    common: 'https://s1.hdslb.com/bfs/static/blive/live-region/libs/area-tags/vue_2.6.14.js',
    framework: 'https://s1.hdslb.com/bfs/static/blive/live-region/libs/area-tags/vue_2.6.14.js'
};

// 规则列表
var rule = {
    common: 'online',
    framework: 'online'
};

// 需要重定向的项
let needRedirectList = []

chrome.runtime.onInstalled.addListener(() => {
    // 请求时回调
    chrome.webRequest.onBeforeRequest.addListener(
        function (details) {
            for(let item of needRedirectList){
                if(details.url.endsWith(item.origin)){
                    return { redirectUrl: item.redirect };
                }
            }
        },
        { 
            types:["script"],
            urls: ["*://*/*.js"] // 进一步过滤js，这里没做进一步筛选
        },
        ["blocking"]
    );
});

// 在弹窗页面改变规则后的回调函数
function changeRule(changeObj) {
    rule = {
        ...rule,
        ...changeObj
    }
    needRedirectList = []
    // 根据新规则设定需要重定向的项
    Object.keys(rule).forEach(ruleName => {
        if (rule[ruleName] === 'local') {
            needRedirectList.push({
                origin:origins[ruleName],
                redirect:redirects[ruleName]
            })
        }
    })
}
