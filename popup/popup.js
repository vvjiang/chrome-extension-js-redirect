let eleRuleList = document.getElementById("rule_list");

let bg = chrome.extension.getBackgroundPage();
// 弹窗页面加载，展现当前的选项
Object.keys(bg.rule).forEach(ruleName=>{
  document.getElementsByName(ruleName).forEach((e)=>{
    if(bg.rule[ruleName]===e.value){
      e.checked=true
    }
  })
})
// 弹窗页改变规则选项
eleRuleList.addEventListener("change", (e) => {
  const changeObj={ [e.target.name]:e.target.value }
  // chrome.storage.sync.set(changeObj); 这里其实可以在storage里记住规则，但是怕忘记了，还是记在内存得了
  var bg = chrome.extension.getBackgroundPage();
  bg.changeRule(changeObj);
});
